# Builds Container for controlling Bittle from Pi to Ardruino over GPIO
# Assumes:
#  1. That the appropriate verions of ardSerial and associated files live next to the Dockerfile. (Collected by another CI job)
#  2. That the Kubernetes host PI is prepped with proper configuration commands.
#  3. That the pod is launched with bittlecontrolpod.yml which maps the proper ports. 

#  TODO:
#  - Does this pull the right arch when running under a generic ARM build machine.

FROM public.ecr.aws/docker/library/python:slim

RUN pip3 install pyserial numpy

RUN mkdir -p serialMaster

COPY serialMaster serialMaster

RUN sed -i "s/\/dev\/cu\.wchusbserial1430/\/dev\/ttyAMA0/g" serialMaster/ardSerial.py
