# This "run-from-web" code can be downloaded and run with this one line script:

# wget https://gitlab.com/sdv.bot/bittle/-/raw/main/rfw-3-bittle-ctrl-pod.sh -O /tmp/setup3.sh ; bash /tmp/setup3.sh

echo "Applying manifest to install bittle control pod..."
echo ""
read -n 1 -s -r -p "It can also be fun to see GitLab Agent perform this install, hit CTRL-C to cancel and perform the GitLab Agent method, or press any key to continue..."
echo ""
wget https://gitlab.com/sdv.bot/bittle/-/raw/main/deploymanifests/bittlecontrolpod.yml -O bittlecontrolpod.yml

kubectl apply -f bittlecontrolpod.yml

kubectl get pods

echo "Keep checking the pod status with 'kubectl get pods' wait until the STATUS column says 'Running' (It can take as much as 5 minutes)"
echo "Then use these two commands to enter the pod and then issue a command to bittle"
echo "  kubectl exec bittlectrl -it -- bash"
echo "  python3 serialMaster/ardSerial.py kbalance"
echo ""
echo "The Bittle should perform the balance instinct and move into a standing position."
echo ""
echo "To make Bittle sit down, use:"
echo "  python3 serialMaster/ardSerial.py ksit"
echo ""
echo "*** There are many more commands at https://docs.petoi.com/apis/serial-protocol"
