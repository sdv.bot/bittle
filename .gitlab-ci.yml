

image: 
  name: gcr.io/kaniko-project/executor:debug
  entrypoint: [""]

variables: 
  DEPLOY_BITTLE_POD:
    value: 'false'
    description: "Deploy or update the Bittle Control Pod rather than building the Bittle Control Image"
  DOCKERFILE_PATH: "."
  #More Information on Kaniko Caching: https://cloud.google.com/build/docs/kaniko-cache
  KANIKO_CACHE_ARGS: "--cache=true --cache-copy-layers=true --cache-ttl=24h"
  VERSIONLABELMETHOD: "OnlyIfThisCommitHasVersion" # options: "OnlyIfThisCommitHasVersion","LastVersionTagInGit"
  IMAGE_LABELS: >
    --label org.opencontainers.image.vendor=$CI_SERVER_URL/$GITLAB_USER_LOGIN
    --label org.opencontainers.image.authors=$CI_SERVER_URL/$GITLAB_USER_LOGIN
    --label org.opencontainers.image.revision=$CI_COMMIT_SHA
    --label org.opencontainers.image.source=$CI_PROJECT_URL
    --label org.opencontainers.image.documentation=$CI_PROJECT_URL
    --label org.opencontainers.image.licenses=$CI_PROJECT_URL
    --label org.opencontainers.image.url=$CI_PROJECT_URL
    --label vcs-url=$CI_PROJECT_URL
    --label com.gitlab.ci.user=$CI_SERVER_URL/$GITLAB_USER_LOGIN
    --label com.gitlab.ci.email=$GITLAB_USER_EMAIL
    --label com.gitlab.ci.tagorbranch=$CI_COMMIT_REF_NAME
    --label com.gitlab.ci.pipelineurl=$CI_PIPELINE_URL
    --label com.gitlab.ci.commiturl=$CI_PROJECT_URL/commit/$CI_COMMIT_SHA
    --label com.gitlab.ci.cijoburl=$CI_JOB_URL
    --label com.gitlab.ci.mrurl=$CI_PROJECT_URL/-/merge_requests/$CI_MERGE_REQUEST_ID

get_bittle_scripts:
  rules:
    - if: '$DEPLOY_BITTLE_POD != "true"'
      when: always
  stage: .pre
  image: 
    name: alpine/git
    entrypoint: [""]
  script:
    - |
      echo "Getting Bittle Python Control Scripts..."
      TEMPCLONEDIR="/tmp/bittle-pi"
      if [[ ! -f ardSerial.py ]]; then
        #PETOIREPO=https://github.com/PetoiCamp/OpenCat.git
        PETOIREPO=https://github.com/pengyou200902/OpenCat.git
        mkdir -p $TEMPCLONEDIR
        cd $TEMPCLONEDIR
        git init
        git remote add -f origin $PETOIREPO
        git config core.sparseCheckout true
        echo "serialMaster/" >> .git/info/sparse-checkout
        git pull origin main
        cp -r serialMaster $CI_PROJECT_DIR
        cd $CI_PROJECT_DIR
        chmod +x serialMaster/ardSerial.py
      fi
  artifacts:
    paths:
      - serialMaster/

build-for-gitlab-project-registry:
  tags: [linux,docker,arm,asg-scheduled]
  rules:
    - if: '$DEPLOY_BITTLE_POD != "true"'
      when: always
  extends: .build_with_kaniko
  environment:
    #This is only here for completeness, since there are no CI CD Variables with this scope, the project defaults are used
    # to push to this projects docker registry
    name: push-to-gitlab-project-registry

deploy_bittle_ctrl_pod:
  tags: [linux,docker,arm,asg-scheduled]
  rules:
    - if: '$DEPLOY_BITTLE_POD == "true"' #must force run the pipeline and change this variable
      when: always
  image:
    name: bitnami/kubectl:latest
    entrypoint: ['']
  variables:
    KUBE_CONTEXT: $CI_PROJECT_PATH:sdvbot
  script:
    - |
      kubectl config get-contexts
      kubectl config use-context $KUBE_CONTEXT
      kubectl apply -f deploymanifests/bittlecontrolpod.yml
      kubectl get pods

.build_with_kaniko:
  #Hidden job to use as an "extends" template
  stage: build
  script:
    - | 
      echo "Building and shipping image to $CI_REGISTRY_IMAGE"
      #Build date for opencontainers
      BUILDDATE="'$(date '+%FT%T%z' | sed -E -n 's/(\+[0-9]{2})([0-9]{2})$/\1:\2/p')'" #rfc 3339 date
      IMAGE_LABELS="$IMAGE_LABELS --label org.opencontainers.image.created=$BUILDDATE --label build-date=$BUILDDATE"
      #Description for opencontainers
      BUILDTITLE=$(echo $CI_PROJECT_TITLE | tr " " "_")
      IMAGE_LABELS="$IMAGE_LABELS --label org.opencontainers.image.title=$BUILDTITLE --label org.opencontainers.image.description=$BUILDTITLE"
      #Add ref.name for opencontainers
      IMAGE_LABELS="$IMAGE_LABELS --label org.opencontainers.image.ref.name=$CI_REGISTRY_IMAGE:$CI_COMMIT_REF_NAME"

      #if [[ ! -z "$DOCKERFILE" ]]; then
      #  cd $DOCKERFILE_PATH=.
      #else
      #  cd $DOCKERFILE_PATH
      #fi

      #Build Version Label and Tag from git tag, LastVersionTagInGit was placed by a previous job artifact
      if [[ "$VERSIONLABELMETHOD" == "LastVersionTagInGit" ]]; then VERSIONLABEL=$(cat VERSIONTAG.txt); fi
      if [[ "$VERSIONLABELMETHOD" == "OnlyIfThisCommitHasVersion" ]]; then VERSIONLABEL=$CI_COMMIT_TAG; fi
      if [[ ! -z "$VERSIONLABEL" ]]; then 
        IMAGE_LABELS="$IMAGE_LABELS --label org.opencontainers.image.version=$VERSIONLABEL"
        ADDITIONALTAGLIST="$ADDITIONALTAGLIST $VERSIONLABEL"
      fi
      
      ADDITIONALTAGLIST="$ADDITIONALTAGLIST $CI_COMMIT_REF_NAME $CI_COMMIT_SHORT_SHA"
      if [[ "$CI_COMMIT_BRANCH" == "$CI_DEFAULT_BRANCH" ]]; then ADDITIONALTAGLIST="$ADDITIONALTAGLIST latest"; fi
      if [[ -n "$ADDITIONALTAGLIST" ]]; then 
        for TAG in $ADDITIONALTAGLIST; do 
          FORMATTEDTAGLIST="${FORMATTEDTAGLIST} --tag $CI_REGISTRY_IMAGE:$TAG "; 
        done; 
      fi
      
      #Reformat Docker tags to kaniko's --destination argument:
      FORMATTEDTAGLIST=$(echo "${FORMATTEDTAGLIST}" | sed s/\-\-tag/\-\-destination/g) 

      echo "Kaniko arguments to run: --context $CI_PROJECT_DIR/$DOCKERFILE_PATH --dockerfile $DOCKERFILE_PATH/Dockerfile $KANIKO_CACHE_ARGS $FORMATTEDTAGLIST $IMAGE_LABELS"
      mkdir -p /kaniko/.docker
      echo "{\"auths\":{\"$CI_REGISTRY\":{\"auth\":\"$(echo -n $CI_REGISTRY_USER:$CI_REGISTRY_PASSWORD | base64)\"}}}" > /kaniko/.docker/config.json
      /kaniko/executor --context $CI_PROJECT_DIR/$DOCKERFILE_PATH --dockerfile $DOCKERFILE_PATH/Dockerfile $KANIKO_CACHE_ARGS $FORMATTEDTAGLIST $IMAGE_LABELS