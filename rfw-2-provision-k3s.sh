# This "run-from-web" code can be downloaded and run with this one line script:

# wget https://gitlab.com/sdv.bot/bittle/-/raw/main/rfw-2-provision-k3s.sh -O /tmp/setup2.sh ; bash /tmp/setup2.sh

curl -sfL https://get.k3s.io | sh -s - --write-kubeconfig-mode 644

curl -fsSL -o get_helm.sh https://raw.githubusercontent.com/helm/helm/main/scripts/get-helm-3 ; chmod 700 get_helm.sh ; ./get_helm.sh

echo "Waiting 2 minutes for K3s to stabilize..."

echo 'export KUBECONFIG=/etc/rancher/k3s/k3s.yaml' >> ~/.profile

export KUBECONFIG=/etc/rancher/k3s/k3s.yaml

sleep 120

echo "Please check the kube-system cluster pods status with 'kubectl get pods -A'"
echo ""