
## 0.0.6 - 2024-04-28
MVC 1 of SOAFEE Stack on Petoi Bittle

### Added

- Final configuration shows 670MB used in htop - so even 2GB boards might work. The Pi 3a+ w/ 512MB - the board that best fit Petoi Bittle - would not complete setup.
- Configuration of Raspberry Pi 5 Hardware (esp Serial Ports and c_groups)
- Raspberry Pi OS Lite (64-Bit) Released: 2024-03-05  
- Provision k3s (version: v1.29.4+k3s1)
- (Optional) Provision GitLab AgentK
- Provision Bittle Control Pod (manually or via AgentK)
- Demo control of Bittle Inside Container
- Builds the publicly published container for Bittle Control.
- Valided software stack loads on Orange Pi Zero 2W 4G with Debian Bullseye Server Linux 6 Kernel (https://drive.google.com/file/d/1L3M1Z6YJNd55E2CCsmfl_XhTca_DZ_02/view?usp=drive_link). This is the only board this small with 4GB and lacking a massive set of full size ports. Hardware interface is not yet physically created so full validation of serial port configuration and command pass through to Bittle. Raspberry Pi does not sell smaller boards with more than 512MB.
