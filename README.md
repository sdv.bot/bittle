
<div alig="center">![sdvbot icon](./images/sdv500.png)</div>

# sdv.bot

Attempts to get SOAFEE Software Defined Vehicle architecture working on the Petoi Bittle robot 

Tested on Raspberry 5 w/ 8GB. With Kubernetes running, the system takes 670MB. So it appears a 2GB Pi would be enough if you aren't going to run many other containers. It wouldn't be surprising if these exact instructions work for any Raspberry Pi 4 as well.

[[_TOC_]]

### Video Overview

[![Video](./images/mvc1video.jpg)](https://url2go.to/sdvbot)

[Watch Video](https://url2go.to/sdvbot)

### Hardware Prep

1. Solder a 2 x 5 connector to the ardruino board of the Petoi Bittle (not "Bittle X") as documented here: https://youtu.be/iGYNq-T1fZc
2. Using the [Raspberry Pi Imager](https://www.raspberrypi.com/software/), prepare an SD card for a Raspberry Pi 5 using the 64-bit Raspberry Pi OS Lite image (no desktop) and [configure it to connect to your wifi network in the imager utility](https://www.raspberrypi.com/documentation/computers/getting-started.html#raspberry-pi-imager).
3. Connect the Raspberry Pi 5 to the 2x5 connector and be sure the Pi is connecting to your network.
4. Power up the Bittle and SSH into the Pi.

### Setup Pi OS and Hardware

1. Run `wget https://gitlab.com/sdv.bot/bittle/-/raw/main/rfw-1-setup-raspberry-pi-5-for-bittle.sh -O /tmp/setup.sh ; bash /tmp/setup.sh` and follow the prompts.

    If you receive any of these errors:

   ```
   ERROR: The certificate of 'gitlab.com' is not trusted.
   ERROR: The certificate of 'gitlab.com' is not yet activated.
   The certificate has not yet been activated
   ```

   Wait for a bit and try again - it takes a bit for certs to be ready.

   Wait for the prompt "Press any key to reboot"

### Provision k3s Kubernetes

1. After reboot, ssh in to the Pi and run `wget https://gitlab.com/sdv.bot/bittle/-/raw/main/rfw-2-provision-k3s.sh -O /tmp/setup2.sh ; bash /tmp/setup2.sh`

### Provision Bittle Control Pod Manually

> **NOTE:** You can skip to the next section titled 'Provision Bittle Control Pod Using GitLab Agent for Kubernetes' to see this done by GitLab Agent for Kubernetes.

1. ssh in to the Pi and run `wget https://gitlab.com/sdv.bot/bittle/-/raw/main/rfw-3-bittle-ctrl-pod.sh -O /tmp/setup3.sh ; bash /tmp/setup3.sh`
2. Skip forward to section 'Testing Controlling Bittle from Pod'

### Testing Controlling Bittle from Pod

1. ssh in to the Pi
2. Check the Bittle control pod status until it reads "Running" under "STATUS" column by using this command: `kubectl get pods`
3. After the Bittle Control Pod is fully running, enter it by running `kubectl exec bittlectrl -it -- bash`
4. From within the Bittle Control Pod, issue the "Stand up" command with `python3 serialMaster/ardSerial.py kbalance` The Bittle should perform the balance instinct and move into a standing position.
5. From within the Bittle Control Pod, issue the "Sit" command with `python3 serialMaster/ardSerial.py ksit`
6. From within the Bittle Control Pod, issue the "Lay down" command with `python3 serialMaster/ardSerial.py krest`

There are many more commands at https://docs.petoi.com/apis/serial-protocol

### Appendix: Provision Bittle Control Pod Using GitLab Agent for Kubernetes

> **NOTE:** To perform this you must make a copy of this project so that you can setup your own Gitlab Agent attached to your own group and project.
>
> **NOTE:** You can perform this after a manual deployment of the bittle control pod and then GitLab Agent will manage the pod to always be deployed. 

1. Import a copy of this repository to a location you have permissions to.

   1. While in 'yourpersonalgroup' **Click** 'New project' (button) and then Click Import project
   2. On the 'Import project' page, **Click** 'Repository by URL'
   3. On the next page, for 'Git repository URL' **Paste** https://gitlab.com/sdv.bot/bittle.git
   4. In 'Project name' **Type** 'sdv.bot'
   5. Near the bottom of the page **Click** 'Create project' (button)
   6. Wait for the import to complete

2. Within the new project `yourpersonalgroup/sdv.bot`

   1. Near the top right of the page **Click** 'Edit' and then **Select** 'Web IDE'

   2. In the Web IDE file selector, **Navigate** to `.gitlab/agents/sdvbot/`

   3. In the Web IDE file selector, *Click* the  file '**config.yaml**'

   4. Update the file in the two places with the comment starting "#REPLACE WITH". The example file is below.

      > **TAKE CARE:** The path in the Web IDE editor has additional path elements before and after the actual project path. If you copy from it, take only the portion between "project/" and "/edit" - with no leading or trailing slashes. The source file is from a root group on Gitlab.com, so the path is a single element. Your path will most likely have multiple levels - be sure to specify the entire path (with no leading or trailing slashes)
      > Dispite the `id:` yaml parameter name, this is the text group path, not the numeric group id.

      ```yaml
      #id: = Full group path without instance url
      # and without leading or trailing slashes.
      # for https://gitlab.com/this/is/an/example, id would be:
      # - id: this/is/an/example
      
      ci_access:
        groups:
        - id: sdv.bot #REPLACE WITH yourpersonalgrouppath to match current location of this file
      
      observability:
        logging:
          level: debug
      ```

         **Important**: The above configuration represents Traditional Runner CD Push access to every project below the root level group <mark class="hlgreen">classgroup</mark> the scope of projects can be made tighter with more group levels by continuing to specify a GitLab group hiearchy like <mark class="hlgreen">classgroup/mysubgroup</mark> Also use the <mark class="hlgreen">projects:</mark> key word instead of <mark class="hlgreen">groups:</mark> if scoping right to a project. Documentation: [Authorize the agent](https://docs.gitlab.com/ee/user/clusters/agent/ci_cd_tunnel.html#authorize-the-agent).

   5. On {the far left icon navigation}, **Click** {the git icon} (three small circles connected by lines)

   6. In *Commit message*, **Type** ‘Configure agent [skip ci]’

   7. **Click** ‘Commit to ’main’‘

   8. *Click* **Create commit...**

   9. **IMPORTANT:** *Select* **Commit to main branch** (non-default selection)

   10. *Click* **Commit**

3. Obtain Register the GitLab Agent for Kubernetes (agentk) by...

   1. On the left navigation, **Click** 'Operate => Kubernetes Clusters'

   2. In the upper right, **Click** 'Connect a cluster'

   3. In _Connect a Kubernetes cluster_, **Select** the drop down with the text 'Select an agent or enter name to create new'

   4. **Select** 'sdvbot'

      > **NOTE:** The next dialog window after pressing 'Register' only appears once - but sure you've successfully registered the agent before closing it, or copy it's information somewhere safe.

   5. **Click** 'Register'

   6. On the new window, next to the code under _Install using Helm (recommended)_, **Click** {the clipboard icon}

   7. SSH into the Pi and

   8. Let helm know where k3s is with this command (type it if you don't want to loose your paste buffer):
      `export KUBECONFIG=/etc/rancher/k3s/k3s.yaml`

   9. Paste the entire agent command and run it.

   10. Back in a web browser, where you left the page "Connect a Kubernetes cluster", **Click** 'Close'

   11. On the underlying page, the "Connection Status" for 'sdvbot' likely reads "Never connected"

   12. Refresh the webpage

   13. "Connection Status" for 'sdvbot' should now say "Connected"

   14. You can see the logs for the installation of the bittle control pod (and all agent activies) using the command when sshed into the Pi  `kubectl logs deployment/sdvbot-gitlab-agent-v2  -n gitlab-agent-sdvbot` To follow the logs, add the parameter `-f`

4. Run the job to deploy the Bittle Control Pod by...

   1. **Click** 'Build => Pipelines'
   2. On the upper left of the page **Click** 'Run pipeline'
   3. On the variable line with the name DEPLOY_BITTLE_POD change the value from 'false' to 'true'
   4. Near the bottom of the page **Click** 'Run pipeline'
   5. When the pipeline completes successfully, return to the section "Testing Controlling Bittle from Pod"